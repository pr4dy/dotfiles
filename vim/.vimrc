let mapleader=" "

packadd! dracula
syntax on
colorscheme dracula

set relativenumber
set termguicolors
set clipboard=unnamed
" Enable use of the mouse for all modes
set mouse=a
" set clipboard=unnamedplus
set wildignore=*.o,*.obj,*.bak,*.exe

set tabstop=4 "sets indent size of tabs
set softtabstop=4 "Soft tabs
set expandtab "Turns tabs into spaces
set shiftwidth=4 "sets auto-indent size
set autoindent "Turns on auto-indenting
set copyindent "Copy the previous indentation on auto indenting
set smartindent "Remembers previous indent when creating new lines

set hlsearch "Highlights search terms
set incsearch "Highlights search terms as you type them"
set showmatch "Highlights matching parentheses"
set ignorecase "Ignores case when searching"
set smartcase "Unless you put some caps in your search term"
set backspace=indent,eol,start
set belloff=all

"mappings

" Toggle Highlight
nnoremap <Leader>h :set hlsearch! hlsearch?<CR>

" Line Movements
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" Better Escape
imap jk <Esc>
imap jj <Esc>

nnoremap <S-C-p> "0p
" Delete without yank
nnoremap <Leader>d "_d
nnoremap x "_x

" Select all
nnoremap <C-a> ggVG

" source $MYVIMRC reloads the saved $MYVIMRC
nmap <Leader>s :source $MYVIMRC<CR>

" opens $MYVIMRC for editing, or use :tabedit $MYVIMRC
nmap <Leader>e :e $MYVIMRC<CR>

" Save file
nmap <C-s> :w<CR>

" Copy file content
nmap <Leader>c :%y*<CR>
