Set-PSReadlineOption -Colors @{
    Command = "#8BE9FD"
    Number = "#50FA7B"
    Keyword = "#FF79C6"
    String = "#F1FA8C"
    Operator = "#FF92DF"
    Parameter = "#FF79C6"
    InlinePrediction = "#777777"
    Variable = "#50FA7B"
}
Set-PSReadLineOption -HistorySearchCursorMovesToEnd
Set-PSReadLineOption -PredictionSource History
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# aliases
Set-Alias which Get-Command
function Custom-Bat {
    Write-Host ""
    bat --style="changes" --paging=never $ARGS
}
Set-Alias cat Custom-Bat
Set-Alias grep Select-String
Set-Alias df Get-PSDrive
Set-Alias wc Measure-Object
Set-Alias find Get-ChildItem
function touch {
  Param(
    [Parameter(Mandatory=$true)]
    [string]$Path
  )

  if (Test-Path -LiteralPath $Path) {
    (Get-Item -Path $Path).LastWriteTime = Get-Date
  } else {
    New-Item -Type File -Path $Path
  }
}
#editor
Set-Alias np notepad
Set-Alias vi vim
Set-Alias st subl
# Network
Set-Alias ipconfig Get-NetIPConfiguration
Set-Alias nslookup Resolve-DnsName
# Git
function glg {git log --graph --abbrev-commit --decorate --format=format:'%C(yellow)%h%C(reset)%C(auto)%d%C(reset) %C(normal)%s%C(reset) %C(#FF79C6)%an%C(reset) %C(blue)(%ar)%C(reset)' --all}
function gaa {git add .}
function gs {git status}
function gcl {git clone $ARGS}
function push {git push origin $ARGS}
# Ls
Set-Alias ls lsd
function l {lsd -l}
function la {lsd -a}
function ll {lsd -la}
function lt {lsd --tree}
# Navigation
function dl {cd ~/Downloads}
function dc {cd ~/Documents}
# Yt-dlp
function yta-aac {yt-dlp --extract-audio --audio-format aac $ARGS }
function yta-best {yt-dlp --extract-audio --audio-format best $ARGS }
function yta-flac {yt-dlp --extract-audio --audio-format flac $ARGS }
function yta-m4a {yt-dlp --extract-audio --audio-format m4a $ARGS }
function yta-mp3 {yt-dlp --extract-audio --audio-format mp3 $ARGS }
function yta-opus {yt-dlp --extract-audio --audio-format opus $ARGS }
function yta-vorbis {yt-dlp --extract-audio --audio-format vorbis $ARGS }
function yta-wav {yt-dlp --extract-audio --audio-format wav $ARGS }
function ytv-best {yt-dlp -f 'bv*[ext=mp4]+ba[ext=m4a]/b[ext=mp4] / bv*+ba/b' -S 'res:1080' $ARGS }
# Misc
function list-env {gci env:* | sort-object name}
function weather { Invoke-RestMethod https://wttr.in/Vandalur }
Set-Alias env list-env

# Startup
fnm env --use-on-cd | Out-String | Invoke-Expression #fnm node version manager
Invoke-Expression (& {
    $hook = if ($PSVersionTable.PSVersion.Major -lt 6) { 'prompt' } else { 'pwd' }
    (zoxide init --hook $hook powershell) -join "`n"
}) #smarter cd
Invoke-Expression (&starship init powershell) #prompt
