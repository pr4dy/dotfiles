# Files and directory
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'
alias cat='echo "";bat --theme="Dracula" --style="changes" --paging=never --italic-text=always'

#ls aliases
alias ls='lsd'
alias l='lsd -l'
alias ll='lsd -la'
alias la='lsd -a'
alias lt='lsd --tree'

#git aliases
alias gaa='git add -A'
alias gcl='git clone'
alias gcm='git add -A && git commit -am'
alias gc='git checkout'
alias gcb='git checkout -b'
alias gb='git branch'
alias gl='git log --graph --abbrev-commit --decorate --format=format:"%C(yellow)%h%C(reset)%C(auto)%d%C(reset) %C(normal)%s%C(reset) %C(#FF79C6)%an%C(reset) %C(blue)(%ar)%C(reset)" --all'
alias gs='git status'
alias gi='git init'

# directory shortcuts
alias dc='cd ~/Documents'
alias dl='cd ~/Downloads'
alias dt='cd ~/Desktop'
alias r='cd ~/Repos'
alias d='cd ~/Repos/dotfiles'

#editors
alias v='vim'
alias nv='nvim'
alias np='notepad'
alias st='subl'

#explorer
alias o='explorer'
alias clr='clear'
alias cls='clear'

#winget
alias wg='winget'

# yt-dlp
alias yta-aac='yt-dlp --extract-audio --audio-format aac'
alias yta-best='yt-dlp --extract-audio --audio-format best'
alias yta-flac='yt-dlp --extract-audio --audio-format flac'
alias yta-m4a='yt-dlp --extract-audio --audio-format m4a'
alias yta-mp3='yt-dlp --extract-audio --audio-format mp3'
alias yta-opus='yt-dlp --extract-audio --audio-format opus'
alias yta-vorbis='yt-dlp --extract-audio --audio-format vorbis'
alias yta-wav='yt-dlp --extract-audio --audio-format wav'
alias ytv-best='yt-dlp -f bestvideo+bestaudio'


eval "$(fnm env)"
eval "$(starship init bash)"
eval "$(zoxide init bash)"
winfetch
