# git aliases
def gaa [] {
    git add .
}
def gs [] {
    git status
}
def gcl [repo: string] {
    git clone $repo
}
def gcm [msg: string] {
    git add ./
    git commit -m $msg
}
def gl [] {
    git log --decorate --oneline --graph
}
def push [branch: string] {
    git push origin $branch
}

#ls
alias ls = lsd
alias l = lsd -l
alias la = lsd -a
alias ll = lsd -la
alias lt = lsd --tree

#crud
alias cat = open --raw
alias man = help
alias grep = where $it =~
alias rr = rm -r

# directory shortcuts
alias dc = cd ~/Documents
alias dl = cd ~/Downloads
alias dt = cd ~/Desktop
alias r = cd ~/Repos
alias d = cd ~/Repos/dotfiles

#general aliases
alias o = explorer
alias v = nvim
alias np = notepad
alias wg = winget
alias clr = clear
alias cls = clear

#network
def ip [] { curl -s ipinfo.io | from json | get ip }

# yt-dlp
def yta-aac [url: string] { yt-dlp --extract-audio --audio-format aac $url }
def yta-best [url: string] { yt-dlp --extract-audio --audio-format best $url }
def yta-flac [url: string] { yt-dlp --extract-audio --audio-format flac $url }
def yta-m4a [url: string] { yt-dlp --extract-audio --audio-format m4a $url }
def yta-mp3 [url: string] { yt-dlp --extract-audio --audio-format mp3 $url }
def yta-opus [url: string] { yt-dlp --extract-audio --audio-format opus $url }
def yta-vorbis [url: string] { yt-dlp --extract-audio --audio-format vorbis $url }
def yta-wav [url: string] { yt-dlp --extract-audio --audio-format wav $url }
def ytv-best [url: string] { yt-dlp -f bestvideo+bestaudio $url }

#zoxide
zoxide init nushell --hook prompt | save ~/.config/.zoxide.nu
source ~/.config/.zoxide.nu

#starship prompt
starship init nu | save ~/.config/starship.nu
source ~/.config/starship.nu

#fnm
fnm env --shell bash | lines | last 5 | str find-replace 'export ' '' | str find-replace -a '"' '' | str find-replace '\\' '\' | split column = | rename name value | load-env
pathvar add $nu.env.FNM_MULTISHELL_PATH

winfetch
